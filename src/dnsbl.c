/************************************************************************
 *   IRC - Internet Relay Chat
 *   Copyright (C) 2013 Plexus Development Team
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include "client.h"
#include "dnsbl.h"
#include "conf.h"
#include "hostmask.h"
#include "s_misc.h"
#include "ircd.h"
#include "send.h"
#include "list.h"
#include "irc_res.h"
#include "s_user.h"
#include "irc_string.h"
#include "memory.h"
#include "log.h"

static void
dnsbl_callback(void *vptr, const struct sockaddr_in *addr, const char *name)
{
  struct DnsblInfo *info = vptr;
  struct Client *cptr = info->cptr;
  struct MaskItem *dline;

  assert(MyConnect(cptr));

  dline = find_dline_conf(&cptr->localClient->ip, AF_INET);

  dlinkDelete(&info->node, &cptr->localClient->dnsbl_queries);
  dlinkDelete(&info->enode, &info->entry->lookups);

  if (addr != NULL && dline == NULL && (addr->sin_addr.s_addr & 0xF0FFFFFF) == 0x7F)
  {
    struct dnsbl_entry *entry = info->entry;
    int result = addr->sin_addr.s_addr >> 24;
    char reason_buffer[512] = "Blacklisted IP";
    const char *current_date = smalldate(CurrentTime);
    int duration = entry->duration ? entry->duration : DEFAULT_DNSBL_DURATION;
    int i, written, len, ip_len, date_len;
    int hit = 0;

    if (!dlink_list_length(&entry->codes))
      hit = 1;
    else
    {
      dlink_node *ptr;
      DLINK_FOREACH(ptr, entry->codes.head)
        if ((*(int *) ptr->data) == result)
        {
          hit = 1;
          break;
        }
    }

    if (hit)
    {
      ++entry->hits;

      dline = conf_make(CONF_DLINE);
      dline->host = xstrdup(cptr->sockhost);
      dline->until = CurrentTime + duration;
      dline->setat = CurrentTime;

      i = 0, written = 0, len = strlen(entry->reason), ip_len = strlen(cptr->sockhost), date_len = strlen(current_date);
      for (; i < len && written < sizeof(reason_buffer) - 1; ++i)
      {
        if (entry->reason[i] == '%' && entry->reason[i + 1] == 'i' && written + ip_len < sizeof(reason_buffer) - 1)
        {
          memcpy(&reason_buffer[written], cptr->sockhost, ip_len);
          written += ip_len;
          ++i;
        }
        else if (entry->reason[i] == '%' && entry->reason[i + 1] == 'd' && written + date_len < sizeof(reason_buffer) - 1)
        {
          memcpy(&reason_buffer[written], current_date, date_len);
          written += date_len;
          ++i;
        }
        else if (entry->reason[i] == '%' && entry->reason[i + 1] == 'c')
        {
          reason_buffer[written++] = result + 48;
          ++i;
        }
        else
          reason_buffer[written++] = entry->reason[i];
      }

      reason_buffer[written++] = 0;

      dline->reason = xstrdup(reason_buffer);

      SetConfDatabase(dline);
      add_conf_by_address(CONF_DLINE, dline);

      ilog(LOG_TYPE_DLINE, "%s added temporary %d min. D-Line for [%s] [%s]",
        me.name, duration / 60, dline->host, dline->reason);
      sendto_realops_flags(UMODE_ALL, L_ALL, SEND_NOTICE,
          "%s added temporary %d min. D-Line for [%s] [%s]",
          me.name, duration / 60,
          dline->host, dline->reason);

      rehashed_klines = 1;
    }
  }

  if ((dline == NULL || dline->type == CONF_EXEMPT) && dlink_list_length(&cptr->localClient->dnsbl_queries) == 0)
  {
    struct ip_entry *ip = find_or_add_ip(&cptr->localClient->ip);
    ip->dnsbl_clear = 1;

    cptr->localClient->registration &= ~REG_NEED_DNSBL;
    if (!cptr->localClient->registration)
      register_local_user(cptr);
  }

  MyFree(info);
}


/*
 * start_dnsbl_lookup
 *
 * inputs       - struct Client *
 * side effects - The client is checked for being on the DNS blacklists.
 */
void
start_dnsbl_lookup(struct Client *cptr)
{
  struct sockaddr_in *addr;
  unsigned long ip;
  struct in_addr lookup_addr;
  char reverse_ip[INET_ADDRSTRLEN];
  dlink_node *ptr;

  assert(MyConnect(cptr));

  if (cptr->localClient->ip.ss.ss_family != AF_INET)
    return;

  if (dlink_list_length(&dnsbl_items) == 0)
    return;

  addr = (struct sockaddr_in *) &cptr->localClient->ip;
  ip = addr->sin_addr.s_addr;

  lookup_addr.s_addr = (ip << 24) | ((ip & 0xFF00) << 8) | ((ip & 0xFF0000) >> 8) | (ip >> 24);

  if (inet_ntop(AF_INET, &lookup_addr, reverse_ip, sizeof(reverse_ip)) == NULL)
    return;

  cptr->localClient->registration |= REG_NEED_DNSBL;

  DLINK_FOREACH(ptr, dnsbl_items.head)
  {
    struct dnsbl_entry *dentry = ptr->data;
    char host_buf[HOSTLEN + 1];
    struct DnsblInfo *info;
    
    info = MyMalloc(sizeof(struct DnsblInfo));
    dlinkAdd(info, &info->node, &cptr->localClient->dnsbl_queries);
    dlinkAdd(info, &info->enode, &dentry->lookups);

    snprintf(host_buf, sizeof(host_buf), "%s.%s", reverse_ip, dentry->name);

    info->cptr = cptr;
    info->entry = dentry;

    gethost_byname_type(&info->query, (dns_callback_fnc) dnsbl_callback, info, host_buf, T_A);
  }
}

static void
cancel_lookup(struct DnsblInfo *info)
{
    delete_resolver_query(&info->query);

    dlinkDelete(&info->node, &info->cptr->localClient->dnsbl_queries);
    dlinkDelete(&info->enode, &info->entry->lookups);

    MyFree(info);
}

/*
 * clear_dnsbl_lookup
 *
 * inputs       - struct Client *
 * side effects - Pending DNS queries for this client will be canceled
 */
void
clear_dnsbl_lookup(struct Client *cptr)
{
  dlink_node *ptr, *next_ptr;

  assert(MyConnect(cptr));

  DLINK_FOREACH_SAFE(ptr, next_ptr, cptr->localClient->dnsbl_queries.head)
  {
    struct DnsblInfo *info = ptr->data;
    cancel_lookup(info);
  }

  cptr->localClient->registration &= ~REG_NEED_DNSBL;
}

void
cancel_blacklist(struct dnsbl_entry *bl)
{
  dlink_node *ptr, *next_ptr;

  DLINK_FOREACH_SAFE(ptr, next_ptr, bl->lookups.head)
  {
    struct DnsblInfo *info = ptr->data;
    cancel_lookup(info);
  }
}

