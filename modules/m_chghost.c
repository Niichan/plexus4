/*
 *  ircd-hybrid: an advanced Internet Relay Chat Daemon(ircd).
 *  m_chghost.c: Allows changing hostname of connected clients.
 *
 *  Copyright (C) 2002 by the past and present ircd coders, and others.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 *
 *  $Id$
 */

#include "stdinc.h"
#include "client.h"
#include "channel.h"
#include "ircd.h"
#include "irc_string.h"
#include "numeric.h"
#include "fdlist.h"
#include "s_bsd.h"
#include "s_serv.h"
#include "send.h"
#include "parse.h"
#include "modules.h"
#include "s_user.h"
#include "hash.h"

static void
me_chghost(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  struct Client *target_p = NULL;

  if(parc < 3)
    return;

  if(!IsServer(source_p))
    return;

  if ((target_p = find_person(client_p, parv[1])) == NULL)
    return;

  if(strlen(parv[2]) > HOSTLEN || !*parv[2] || !valid_hostname(parv[2]))
    return;

  if(strcmp(target_p->host, parv[2]))
    user_set_hostmask(target_p, NULL, parv[2]);
}

static void
mo_chghost(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  struct Client *target_p = NULL;

  if (!HasUMode(source_p, UMODE_NETADMIN))
  {
    sendto_one(source_p, form_str(ERR_NOPRIVILEGES), me.name, source_p->name);
    return;
  }

  if(parc < 3)
  { 
    sendto_one(source_p, form_str(ERR_NEEDMOREPARAMS), me.name, source_p->name, "CHGHOST"); 
    return;
  }

  target_p = find_person(client_p, parv[1]);
  if (target_p == NULL)
  {
    sendto_one(source_p, form_str(ERR_NOSUCHNICK),
         me.name, source_p->name, parv[1]);
    return;
  } 

  if(strlen(parv[2]) > HOSTLEN)
  {
    sendto_one(source_p, form_str(ERR_CANNOTDOCOMMAND),
         me.name, source_p->name, "CHGHOST", "Hostname is too long");
    return;
  }

  if(!*parv[2] || !valid_hostname(parv[2]))
  {
    sendto_one(source_p, form_str(ERR_CANNOTDOCOMMAND),
         me.name, source_p->name, "CHGHOST", "Illegal character in hostname");
    return;
  }

  sendto_server(NULL, CAP_ENCAP, NOCAPS,
          ":%s ENCAP * CHGHOST %s %s",
          me.name, target_p->name, parv[2]);
  
  if(strcmp(target_p->host, parv[2]))
    user_set_hostmask(target_p, NULL, parv[2]);
}

static struct Message chghost_msgtab = {
  "CHGHOST", 0, 0, 2, MAXPARA, MFLG_SLOW, 0,
  {m_unregistered, m_ignore, m_ignore, me_chghost, mo_chghost, m_ignore}
};

static void
module_init(void)
{
  mod_add_cmd(&chghost_msgtab);
}

static void
module_exit(void)
{
  mod_del_cmd(&chghost_msgtab);
}

struct module module_entry = {
  .node    = { NULL, NULL, NULL },
  .name    = NULL,
  .version = "$Revision$",
  .handle  = NULL,
  .modinit = module_init,
  .modexit = module_exit,
  .flags   = 0
};

